#include "Colors.h"

glm::vec3 colors[] = {
	   glm::vec3(0.145f, 0.90f, 0.35f),		// 0 Green
	   glm::vec3(0.89f, 0.14f, 0.55f),		// 1 Pink
	   glm::vec3(0.23f, 0.14f, 0.89f),		// 2 Blue
	   glm::vec3(0.96f, 0.67f, 0.07f),		// 3 Gold
	   glm::vec3(0.80f, 0.00f, 0.20f),		// 4 Red
	   glm::vec3(0.50f, 0.10f, 0.60f),		// 5 Purple
	   glm::vec3(0.60f, 0.30f, 0.10f),		// 6 Brown
	   glm::vec3(0.50f, 0.50f, 0.60f),		// 7 Gray
	   glm::vec3(0.098f, 0.403f, 0.843f)    // 8 Blue2
};