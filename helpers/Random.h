#pragma once

#include <random>

extern int randNumber(int min, int max);
extern float randNumberDouble(double min, double max);
extern float roundFloat(float var);